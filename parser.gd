# SPDX-FileCopyrightText: 2020 Adriel Dumas--Jondeau <adrieldj@orange.fr>
# SPDX-License-Identifier: MIT
extends Node2D


enum {
	Say,
	Prompt,
	Move,

	If, Equals,
	And, Or, Not,
	Const, Get, Set,
}


func parse(action: Array) -> Dictionary:
	match action:
		[Say, var action]:
			var ret = parse(action)
			if ret.has("err"):
				return ret
			print(ret.ret)
			return { ret = {} }

		[Prompt, var action]:
			var ret = parse(action)
			if ret.has("err"):
				return ret
			print(ret.ret)
			return { ret = false }

		[Move, var struct]:
			print("Je me rends sur la case", struct)
			return { ret = {} }

		[If, var cond, var then_body, var else_body]:
			var ret = parse(cond)
			if ret.has("err"):
				return ret
			if ret.ret == true:
				return step(then_body)
			else:
				return step(else_body)

		[Equals, var left, var right]:
			return {
				ret = parse(left).ret == parse(right).ret
			}
		[And, var left, var right]:
			return {
				ret = parse(left).ret and parse(right).ret
			}
		[Or, var left, var right]:
			return {
				ret = parse(left).ret or parse(right).ret
			}
		[Not, var boolean]:
			return { ret = not parse(boolean).ret }

		[Const, var value]:
			return { ret = value }

		[Get, var string]:
			var ret = parse(string)
			if ret.has("err"):
				return ret
			var value = get(ret.ret)
			if value == null:
				return { err = "%s isn't a global variable." % ret.ret }
			return { ret = value }
		[Set, var property, var value]:
			var name = parse(property)
			if name.has("err"):
				return name
			var val = parse(value)
			if val.has("err"):
				return val
			set(name.ret, val.ret)
			return { ret = {} }

		[]:
			return { err = "No command to execute." }
		var error:
			return { err = "Unsupported operation %s." % error }


# Immediately stops in case of error
func step(actions: Array) -> Dictionary:
	for action in actions:
		var ret = parse(action)
		if ret.has("err"):
			return ret
	return { ret = {} }


const actions = [
	[Say, [Const, "Bonjour, aventurière !"]],
	[Say, [Const, "Je serai votre guide tout au long de cet aventure…"]],
	[
		If, [Prompt, [Const, "Connaissez-vous le principe du jeu ?"]],
		[
			[Say, [Const, "Nous pouvons donc commencer sans plus tarder."]],
		],
		[
			[Say, [Const, "Je me dois donc de vous l’expliquer !"]],
			[Say, [Const, "Blah blah blah, encore plus d’explications…"]],
		],
	],
	[Prompt, [Const, "Êtes-vous prête à commencer l’aventure ?\n"]]
]


const including_some_other_action = [
	If, [Equals, [Const, false], [Const, not false]],
	[
		[Say, [Const, "Unreachable!"]]
	],
	[
		[Say, [Const, "Now i will give you the full code of myself:"]],
		[Say, [Get, [Const, "including_some_other_action"]]],
		[Say, [Const, "This works because I'm declared as global!\n"]],
	],
]


func _ready():
	# warning-ignore:return_value_discarded
	parse([
		If, [Equals, [Const, "shit"], [Const, "shit"]],
		[
			[Say, [Const, "It worked!!!"]],
			including_some_other_action,
		],
		[
			[Say, [Const, "Unreachable!"]],
		],
	])

	# warning-ignore:return_value_discarded
	step(actions)

	# Check for proper behavior when common errors are occuring
	assert(
		parse([]).err == "No command to execute."
	)
